# ColdComplexSystems

This repository contains data, codes and snippets for project "Cold collisions of atoms and molecules with complex structure". 



## Potential energy curves / surfaces
The directories with pes_XXXXX contain appropriate Potential Energy Surfaces.
The naming convention: lower case, snake_case: eg li_o2.



## Molscat routines
The naming convention vstar_xxxx.f90. For base9 routines that's base9_xxx.f90

## Code snippets

I plan to add here some pieces of codes 

## License
These data are completely fre to use

## Project status

This project is living forever. I will keep it updated in case there is something new in it
